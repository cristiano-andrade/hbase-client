package com.dudegov.hbase.client;

import java.util.List;

public interface HBaseClient {

    void put(String tableName, String row, List<Column> columns) throws HBaseClientException;

    Row get(String tableName, String row) throws HBaseClientException;

    void delete(String tableName, String row) throws HBaseClientException;

    List<Row> list(String tableName) throws HBaseClientException;
}
