package com.dudegov.hbase.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Row implements Serializable {
    private String rowKey;
    private List<Column> columns;

    public String getRowKey() {
        return rowKey;
    }

    public Row(String rowKey, List<Column> columns) {
        this.rowKey = rowKey;
        this.columns = columns;
    }

    public List<Column> getColumns() {
        if (columns == null) {
            columns = new ArrayList<Column>();
        }
        return columns;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(getRowKey()).append(": {\n");

        for (Column column : getColumns()) {
            sb.append('\t');

            sb.append("'").append(column.getColumnDescriptor().toString()).append("'").append(":").append(column.getValue()).append(",\n");
        }

        sb = sb.deleteCharAt(sb.lastIndexOf(","));

        sb.append("}");

        return sb.toString();
    }
}
