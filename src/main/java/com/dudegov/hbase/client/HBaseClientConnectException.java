package com.dudegov.hbase.client;

public class HBaseClientConnectException extends  Exception {
   public HBaseClientConnectException(Throwable e){
       super(e);
   }
}
