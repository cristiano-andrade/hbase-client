package com.dudegov.hbase.client;

public class HBaseClientException extends  Exception {
   public HBaseClientException(Throwable e){
       super(e);
   }
}
