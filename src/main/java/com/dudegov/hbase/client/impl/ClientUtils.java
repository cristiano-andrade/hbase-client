package com.dudegov.hbase.client.impl;

import com.dudegov.hbase.client.Column;
import com.dudegov.hbase.client.ColumnDescriptor;
import org.apache.hadoop.hbase.thrift2.generated.TColumnValue;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

class ClientUtils {

    private ClientUtils() {
    }

    public static ByteBuffer toBytes(String value) {
        return ByteBuffer.wrap(value.getBytes());
    }

    public static TColumnValue toColumnValue(Column column) {
        TColumnValue result = new TColumnValue();
        result.setFamily(toBytes(column.getColumnDescriptor().getFamily()));
        result.setQualifier(toBytes(column.getColumnDescriptor().getName()));
        result.setValue(toBytes(column.getValue()));
        return result;
    }

    public static List<TColumnValue> toColumnValues(List<Column> columns) {
        List<TColumnValue> result = new ArrayList<TColumnValue>(columns.size());
        for (Column column : columns) {
            result.add(toColumnValue(column));
        }
        return result;
    }

    public static Column toColumn(TColumnValue columnValue) {

        Column column = new Column(new ColumnDescriptor(
                toUTF8String(columnValue.getFamily()),
                toUTF8String(columnValue.getQualifier())
        ),
                toUTF8String(columnValue.getValue()));

        return column;
    }

    public static List<Column> toColumns(List<TColumnValue> columnValues) {
        List<Column> result = new ArrayList<Column>(columnValues.size());

        for (TColumnValue columnValue : columnValues) {
            result.add(toColumn(columnValue));
        }

        return result;
    }

    public static String toUTF8String(byte[] value) {
        try {
            return new String(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String toUTF8String(ByteBuffer byteBuffer) {
        return toUTF8String(byteBuffer.array());
    }

}
