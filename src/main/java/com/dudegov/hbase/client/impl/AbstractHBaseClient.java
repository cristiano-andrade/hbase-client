package com.dudegov.hbase.client.impl;

import com.dudegov.hbase.client.HBaseClientConnectException;

public abstract class AbstractHBaseClient {

    private boolean open = false;

    private String host;
    private int port;

    public AbstractHBaseClient(String host, int port){
         this.host = host;
         this.port = port;
    }

    protected abstract void connect() throws HBaseClientConnectException;

    public void open() throws HBaseClientConnectException{
        open = true;
        connect();
    }

    public void  close(){
        open = false;
        shutdown();
    }

    protected abstract void shutdown();

    public boolean  isOpen(){
        return this.open;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
