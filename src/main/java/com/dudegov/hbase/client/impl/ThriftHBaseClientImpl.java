package com.dudegov.hbase.client.impl;

import com.dudegov.hbase.client.*;
import org.apache.hadoop.hbase.thrift2.generated.*;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ThriftHBaseClientImpl extends AbstractHBaseClient implements HBaseClient {

    private THBaseService.Iface client;
    private TTransport transport;
    private final Logger logger = LoggerFactory.getLogger(ThriftHBaseClientImpl.class);

    public ThriftHBaseClientImpl(String host, int port) {
        super(host, port);
    }

    @Override
    protected void connect() throws HBaseClientConnectException {

        transport = new TSocket(getHost(), getPort());

        TProtocol protocol = new TBinaryProtocol(transport, true, true);

        client = new THBaseService.Client(protocol);

        try {
            logger.info("Connecting to ThriftServer: " + getHost() + ":" + getPort());
            transport.open();
            logger.info("Connected to ThriftServer: " + getHost() + ":" + getPort());
        } catch (TTransportException e) {
            logger.error("Error while connecting to ThriftServer: " + getHost() + ":" + getPort());
            throw new HBaseClientConnectException(e);
        }
    }

    @Override
    protected void shutdown() {
        logger.info("Shutting down client.");
        transport.close();
        logger.info("Client was closed.");
    }

    @Override
    public void put(String tableName, String row, List<Column> columns) throws HBaseClientException {
        logger.info("Put " + columns.toString() + " to row " + row);
        TPut put = new TPut(ClientUtils.toBytes(row), ClientUtils.toColumnValues(columns));

        try {
            client.put(ClientUtils.toBytes(tableName), put);
            logger.info("Row " + row + " successfully put.");
        } catch (Exception e) {
            logger.error("Error while put row " + row + ": " + e.getMessage());
            throw new HBaseClientException(e);
        }
    }

    @Override
    public Row get(String tableName, String row) throws HBaseClientException {
        logger.info("Get row: " + row);
        TGet get = new TGet(ClientUtils.toBytes(row));

        try {
            TResult result = client.get(ClientUtils.toBytes(tableName), get);
            logger.info("Row " + row + " successfully get.");
            return new Row(ClientUtils.toUTF8String(result.getRow()), ClientUtils.toColumns(result.getColumnValues()));
        } catch (Exception e) {
            logger.error("Error while get row " + row + ": " + e.getMessage());
            throw new HBaseClientException(e);
        }
    }

    @Override
    public void delete(String tableName, String row) throws HBaseClientException {
        logger.info("Delete row: " + row);
        TDelete delete = new TDelete(ClientUtils.toBytes(row));

        try {
            client.deleteSingle(ClientUtils.toBytes(tableName), delete);
            logger.info("Row " + row + " was deleted.");
        } catch (Exception e) {
            logger.error("Error while delete row " + row + ": " + e.getMessage());
            throw new HBaseClientException(e);
        }

    }

    @Override
    public List<Row> list(String tableName) throws HBaseClientException {

        TScan tScan = new TScan();

        List<Row> rows = new ArrayList<Row>();

        try {
            int scannerId = client.openScanner(ClientUtils.toBytes(tableName), tScan);

            List<TResult> results = client.getScannerRows(scannerId,1000);

            for(TResult result : results) {
                Row row = new Row(ClientUtils.toUTF8String(result.getRow()), ClientUtils.toColumns(result.getColumnValues()));
                rows.add(row);
            }

            client.closeScanner(scannerId);

            return  rows;

        } catch (Exception e) {
            logger.error("Error while scan table " + tableName + ": " + e.getMessage());
            throw new HBaseClientException(e);
        }

    }
}
