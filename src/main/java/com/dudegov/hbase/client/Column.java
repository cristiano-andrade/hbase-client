package com.dudegov.hbase.client;

public class Column {
    private ColumnDescriptor columnDescriptor;
    private String value;

    public Column(ColumnDescriptor columnDescriptor, String value){
        this.columnDescriptor = columnDescriptor;
        this.value = value;
    }

    public ColumnDescriptor getColumnDescriptor() {
        return columnDescriptor;
    }

    public void setColumnDescriptor(ColumnDescriptor columnDescriptor) {
        this.columnDescriptor = columnDescriptor;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Column {" +
                "columnDescriptor=" + columnDescriptor +
                ", value='" + value + '\'' +
                '}';
    }
}
