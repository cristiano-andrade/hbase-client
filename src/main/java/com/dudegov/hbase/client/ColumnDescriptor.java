package com.dudegov.hbase.client;

import org.apache.commons.lang.StringUtils;

public class ColumnDescriptor {
    private String family;
    private  String name;

    public ColumnDescriptor(String family, String name) {
        this.family = family;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    @Override
    public String toString(){
        if(StringUtils.isNotEmpty(this.family)){
            return this.family + ":" + this.name;
        } else {
            return this.name;
        }
    }
}
