package com.dudegov.hbase.client;

import com.dudegov.hbase.client.impl.ThriftHBaseClientImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ThriftHBaseClientTest {

    final ThriftHBaseClientImpl thriftHBaseClient = new ThriftHBaseClientImpl("localhost", 9090);

    @Before
    @Ignore
    public void setUp() throws  HBaseClientConnectException {
        thriftHBaseClient.open();
    }

    @Test
    @Ignore
    public void testPut() {
        List<Column> columns = new ArrayList<Column>();

        columns.add(new Column(new ColumnDescriptor("info", "name"), "Cristiano Andrade"));
        columns.add(new Column(new ColumnDescriptor("info", "nickName"), "Barrinha"));
        columns.add(new Column(new ColumnDescriptor("info", "city"), "Barrinha"));
        columns.add(new Column(new ColumnDescriptor("info", "street"), "Rua Rafael Brunini"));
        columns.add(new Column(new ColumnDescriptor("info", "number"), "269"));

        try {
            thriftHBaseClient.put("MyTable", "profile-1", columns);
            thriftHBaseClient.put("MyTable", "profile-2", columns);
            thriftHBaseClient.put("MyTable", "profile-3", columns);
            thriftHBaseClient.put("MyTable", "profile-4", columns);
        } catch (HBaseClientException e) {
            e.printStackTrace();
        }

    }

    @Test
    @Ignore
    public void testGet() {
        try {
            Row row = thriftHBaseClient.get("MyTable", "profile-1");
            System.out.println(row);
        } catch (HBaseClientException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Ignore
    public void testDelete() {
        try {
            thriftHBaseClient.delete("MyTable", "profile-1");
        } catch (HBaseClientException e) {
            e.printStackTrace();
        }
    }

    @After
    @Ignore
    public void shutdown() {
        if (thriftHBaseClient.isOpen()) {
            thriftHBaseClient.close();
        }
    }
}
